/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-wire.h is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-wire.h is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-wire.h. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 *          Zhu Li <phillipszhuli1990@gmail.com>
 */

#ifndef NNN_WIRE_H
#define NNN_WIRE_H

#include "ns3/buffer.h"

//#include "ns3/nnn-common.h"
#include "ns3/nnn-naming.h"
#include "ns3/nnn-pdus.h"

NNN_NAMESPACE_BEGIN

struct Wire
{
  enum
  {
    WIRE_FORMAT_DEFAULT = -2,
    WIRE_FORMAT_AUTODETECT = -1,
    WIRE_FORMAT_NNNSIM = 0
  };

  static Packet*
  FromNULLp (const NULLp* n_o, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static NULLp*
  ToNULLp (Packet* packet, int8_t type = WIRE_FORMAT_AUTODETECT);

  static Packet*
  FromSO (const SO* so_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static SO*
  ToSO (Packet* packet, int8_t type = WIRE_FORMAT_AUTODETECT);

  static Packet*
  FromDO (const DO* do_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static DO*
  ToDO (Packet* packet, int8_t type = WIRE_FORMAT_AUTODETECT);

  static Packet*
  FromEN (const EN* n_o, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static EN*
  ToEN (Packet* packet, int8_t type = WIRE_FORMAT_AUTODETECT);

  static Packet*
  FromAEN (const AEN* n_o, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static AEN*
  ToAEN (Packet* packet, int8_t type = WIRE_FORMAT_AUTODETECT);

  static Packet*
  FromREN (const REN* n_o, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static REN*
  ToREN (Packet* packet, int8_t type = WIRE_FORMAT_AUTODETECT);

  static Packet*
  FromDEN (const DEN* n_o, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static DEN*
  ToDEN (Packet* packet, int8_t type = WIRE_FORMAT_AUTODETECT);

  static Packet*
  FromINF (const INF* n_o, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static INF*
  ToINF (Packet* packet, int8_t type = WIRE_FORMAT_AUTODETECT);

  static Packet*
  FromDU (const DU* du_o, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static DU*
  ToDU (Packet* packet, int8_t type = WIRE_FORMAT_AUTODETECT);

  static Packet*
  FromOEN (const OEN* du_o, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static OEN*
  ToOEN (Packet* packet, int8_t type = WIRE_FORMAT_AUTODETECT);

  // Helper methods for Python
  static std::string
  FromNULLpStr (const NULLp* so_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static NULLp*
  ToNULLpStr (const std::string &wire, int8_t type = WIRE_FORMAT_AUTODETECT);

  static std::string
  FromSOStr (const SO* so_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static SO*
  ToSOStr (const std::string &wire, int8_t type = WIRE_FORMAT_AUTODETECT);

  static std::string
  FromDOStr (const DO* do_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static DO*
  ToDOStr (const std::string &wire, int8_t type = WIRE_FORMAT_AUTODETECT);

  static std::string
  FromDUStr (const DU* du_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static DU*
  ToDUStr (const std::string &wire, int8_t type = WIRE_FORMAT_AUTODETECT);

  static std::string
  FromENStr (const EN* en_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static EN*
  ToENStr (const std::string &wire, int8_t type = WIRE_FORMAT_AUTODETECT);

  static std::string
  FromAENStr (const AEN* aen_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static AEN*
  ToAENStr (const std::string &wire, int8_t type = WIRE_FORMAT_AUTODETECT);

  static std::string
  FromRENStr (const REN* ren_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static REN*
  ToRENStr (const std::string &wire, int8_t type = WIRE_FORMAT_AUTODETECT);

  static std::string
  FromDENStr (const DEN* den_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static DEN*
  ToDENStr (const std::string &wire, int8_t type = WIRE_FORMAT_AUTODETECT);

  static std::string
  FromINFStr (const INF* inf_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static INF*
  ToINFStr (const std::string &wire, int8_t type = WIRE_FORMAT_AUTODETECT);

  static std::string
  FromOENStr (const OEN* du_p, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  static OEN*
  ToOENStr (const std::string &wire, int8_t type = WIRE_FORMAT_AUTODETECT);

  // /*
  //  * @brief Get size of buffer to fit wire-formatted name object
  //  */
  // static uint32_t
  // FromNameSize (Ptr<const Name> name, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  /**
   * @brief Convert name to wire format
   */
  static std::string
  FromName (const NNNAddress* name, int8_t wireFormat = WIRE_FORMAT_DEFAULT);

  /**
   * @brief Convert name from wire format
   */
  static NNNAddress*
  ToName (const std::string &wire, int8_t wireFormat = WIRE_FORMAT_DEFAULT);
};

inline std::string
PacketToBuffer (Packet* pkt)
{
  std::string buffer;
  buffer.resize (pkt->GetSize ());
  pkt->CopyData (reinterpret_cast<uint8_t*> (&buffer[0]), buffer.size ());

  return buffer;
}

inline Packet*
BufferToPacket (const std::string &buffer)
{
  return Packet* (reinterpret_cast<const uint8_t*> (&buffer[0]), buffer.size ());
}

NNN_NAMESPACE_END

#endif // NNN_WIRE_H

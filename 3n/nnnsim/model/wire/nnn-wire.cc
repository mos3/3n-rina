/* -*- Mode: C++; c-file-style: "gnu" -*- */
/*
 * Copyright (c) 2015 Waseda University, Sato Laboratory
 *
 *   This file is part of nnnsim.
 *
 *  nnn-wire.cc is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  nnn-wire.cc is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with nnn-wire.cc. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Jairo Eduardo Lopez <jairo@ruri.waseda.jp>
 *          Zhu Li <phillipszhuli1990@gmail.com>
 */

#include "ns3/integer.h"
#include "ns3/global-value.h"

#include "ns3/nnn-header-helper.h"
#include "nnn-wire.h"
#include "ns3/wire-nnnsim.h"

//#include "ns3/nnnsim-aen.h"
//#include "ns3/nnnsim-den.h"
//#include "ns3/nnnsim-do.h"
//#include "ns3/nnnsim-du.h"
//#include "ns3/nnnsim-en.h"
//#include "ns3/nnnsim-inf.h"
//#include "ns3/nnnsim-nullp.h"
//#include "ns3/nnnsim-oen.h"
//#include "ns3/nnnsim-ren.h"
#include "ns3/nnnsim-so.h"

NNN_NAMESPACE_BEGIN

static
GlobalValue g_wireFormat ("nnn::WireFormat",
                          "Default wire format for nnnSIM.  nnnSIM will be accepting packets "
                          "in any supported packet formats, but if encoding requested, it will "
                          "use default wire format to encode (0 for nnnSIM (default))",
                          IntegerValue (Wire::WIRE_FORMAT_NNNSIM),
                          MakeIntegerChecker<int32_t> ());

static inline uint32_t
GetWireFormat ()
{
  static int32_t format = -1;
  if (format >= 0)
    return format;

  IntegerValue value;
  g_wireFormat.GetValue (value);
  format = value.Get ();

  return format;
}

Packet*
Wire::FromNULLp (const NULLp* n_o, int8_t wireFormat)
{
  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    return wire::nnnSIM::NULLp::ToWire (n_o);
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
      return 0;
    }
}

NULLp*
Wire::ToNULLp (Packet* packet, int8_t wireFormat)
{
  if (wireFormat == WIRE_FORMAT_AUTODETECT)
    {
      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::NULL_NNN:
	      {
		return wire::nnnSIM::NULLp::FromWire (packet);
	      }
	    default:
	      NS_FATAL_ERROR ("Unsupported format");
	      return 0;
	  }

	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown NNN header");
	  return 0;
      }
    }
  else
    {
      if (wireFormat == WIRE_FORMAT_NNNSIM)
	return wire::nnnSIM::NULLp::FromWire (packet);
      else
	{
	  NS_FATAL_ERROR ("Unsupported format requested");
	  return 0;
	}
    }
}

Packet*
Wire::FromSO (const SO* so_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    return wire::nnnSIM::SO::ToWire (so_p);
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
      return 0;
    }
}

SO*
Wire::ToSO (Packet* packet, int8_t wireFormat/* = WIRE_FORMAT_AUTODETECT*/)
{
  if (wireFormat == WIRE_FORMAT_AUTODETECT)
    {
      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::SO_NNN:
	      {
		return wire::nnnSIM::SO::FromWire (packet);
	      }
	    default:
	      NS_FATAL_ERROR ("Unsupported format");
	      return 0;
	  }

	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown NNN header");
	  return 0;
      }
    }
  else
    {
      if (wireFormat == WIRE_FORMAT_NNNSIM)
	return wire::nnnSIM::SO::FromWire (packet);
      else
	{
	  NS_FATAL_ERROR ("Unsupported format requested");
	  return 0;
	}
    }
}

Packet*
Wire::FromDO (const DO* do_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    return wire::nnnSIM::DO::ToWire (do_p);
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
      return 0;
    }
}

DO*
Wire::ToDO (Packet* packet, int8_t wireFormat/* = WIRE_FORMAT_AUTODETECT*/)
{
  if (wireFormat == WIRE_FORMAT_AUTODETECT)
    {
      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::DO_NNN:
	      {
		return wire::nnnSIM::DO::FromWire (packet);
	      }
	    default:
	      NS_FATAL_ERROR ("Unsupported format");
	      return 0;
	  }

	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown NNN header");
	  return 0;
      }
    }
  else
    {
      if (wireFormat == WIRE_FORMAT_NNNSIM)
	return wire::nnnSIM::DO::FromWire (packet);
      else
	{
	  NS_FATAL_ERROR ("Unsupported format requested");
	  return 0;
	}
    }
}

Packet*
Wire::FromEN (const EN* en_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    return wire::nnnSIM::EN::ToWire (en_p);
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
      return 0;
    }
}

EN*
Wire::ToEN (Packet* packet, int8_t wireFormat/* = WIRE_FORMAT_AUTODETECT*/)
{
  if (wireFormat == WIRE_FORMAT_AUTODETECT)
    {
      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::EN_NNN:
	      {
		return wire::nnnSIM::EN::FromWire (packet);
	      }
	    default:
	      NS_FATAL_ERROR ("Unsupported format");
	      return 0;
	  }

	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown NNN header");
	  return 0;
      }
    }
  else
    {
      if (wireFormat == WIRE_FORMAT_NNNSIM)
	return wire::nnnSIM::EN::FromWire (packet);
      else
	{
	  NS_FATAL_ERROR ("Unsupported format requested");
	  return 0;
	}
    }
}

Packet*
Wire::FromAEN (const AEN* aen_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    return wire::nnnSIM::AEN::ToWire (aen_p);
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
      return 0;
    }
}

AEN*
Wire::ToAEN (Packet* packet, int8_t wireFormat/* = WIRE_FORMAT_AUTODETECT*/)
{
  if (wireFormat == WIRE_FORMAT_AUTODETECT)
    {
      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::AEN_NNN:
	      {
		return wire::nnnSIM::AEN::FromWire (packet);
	      }
	    default:
	      NS_FATAL_ERROR ("Unsupported format");
	      return 0;
	  }

	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown NNN header");
	  return 0;
      }
    }
  else
    {
      if (wireFormat == WIRE_FORMAT_NNNSIM)
	return wire::nnnSIM::AEN::FromWire (packet);
      else
	{
	  NS_FATAL_ERROR ("Unsupported format requested");
	  return 0;
	}
    }
}

Packet*
Wire::FromREN (const REN* ren_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    return wire::nnnSIM::REN::ToWire (ren_p);
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
      return 0;
    }
}

REN*
Wire::ToREN (Packet* packet, int8_t wireFormat/* = WIRE_FORMAT_AUTODETECT*/)
{
  if (wireFormat == WIRE_FORMAT_AUTODETECT)
    {
      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::REN_NNN:
	      {
		return wire::nnnSIM::REN::FromWire (packet);
	      }
	    default:
	      NS_FATAL_ERROR ("Unsupported format");
	      return 0;
	  }

	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown NNN header");
	  return 0;
      }
    }
  else
    {
      if (wireFormat == WIRE_FORMAT_NNNSIM)
	return wire::nnnSIM::REN::FromWire (packet);
      else
	{
	  NS_FATAL_ERROR ("Unsupported format requested");
	  return 0;
	}
    }
}

Packet*
Wire::FromDEN (const DEN* ren_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    return wire::nnnSIM::DEN::ToWire (ren_p);
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
      return 0;
    }
}

DEN*
Wire::ToDEN (Packet* packet, int8_t wireFormat/* = WIRE_FORMAT_AUTODETECT*/)
{
  if (wireFormat == WIRE_FORMAT_AUTODETECT)
    {
      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::DEN_NNN:
	      {
		return wire::nnnSIM::DEN::FromWire (packet);
	      }
	    default:
	      NS_FATAL_ERROR ("Unsupported format");
	      return 0;
	  }

	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown NNN header");
	  return 0;
      }
    }
  else
    {
      if (wireFormat == WIRE_FORMAT_NNNSIM)
	return wire::nnnSIM::DEN::FromWire (packet);
      else
	{
	  NS_FATAL_ERROR ("Unsupported format requested");
	  return 0;
	}
    }
}

Packet*
Wire::FromINF (const INF* inf_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    return wire::nnnSIM::INF::ToWire (inf_p);
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
      return 0;
    }
}

INF*
Wire::ToINF (Packet* packet, int8_t wireFormat/* = WIRE_FORMAT_AUTODETECT*/)
{
  if (wireFormat == WIRE_FORMAT_AUTODETECT)
    {
      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::INF_NNN:
	      {
		return wire::nnnSIM::INF::FromWire (packet);
	      }
	    default:
	      NS_FATAL_ERROR ("Unsupported format");
	      return 0;
	  }

	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown NNN header");
	  return 0;
      }
    }
  else
    {
      if (wireFormat == WIRE_FORMAT_NNNSIM)
	return wire::nnnSIM::INF::FromWire (packet);
      else
	{
	  NS_FATAL_ERROR ("Unsupported format requested");
	  return 0;
	}
    }
}

Packet*
Wire::FromDU (const DU* du_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    return wire::nnnSIM::DU::ToWire (du_p);
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
      return 0;
    }
}

DU*
Wire::ToDU (Packet* packet, int8_t wireFormat/* = WIRE_FORMAT_AUTODETECT*/)
{
  if (wireFormat == WIRE_FORMAT_AUTODETECT)
    {
      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::DU_NNN:
	      {
		return wire::nnnSIM::DU::FromWire (packet);
	      }
	    default:
	      NS_FATAL_ERROR ("Unsupported format");
	      return 0;
	  }

	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown NNN header");
	  return 0;
      }
    }
  else
    {
      if (wireFormat == WIRE_FORMAT_NNNSIM)
	return wire::nnnSIM::DU::FromWire (packet);
      else
	{
	  NS_FATAL_ERROR ("Unsupported format requested");
	  return 0;
	}
    }
}

Packet*
Wire::FromOEN (const OEN* oen_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    return wire::nnnSIM::OEN::ToWire (oen_p);
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
      return 0;
    }
}

OEN*
Wire::ToOEN (Packet* packet, int8_t wireFormat/* = WIRE_FORMAT_AUTODETECT*/)
{
  if (wireFormat == WIRE_FORMAT_AUTODETECT)
    {
      try
      {
	  NNN_PDU_TYPE type = HeaderHelper::GetNNNHeaderType (packet);
	  switch (type)
	  {
	    case nnn::OEN_NNN:
	      {
		return wire::nnnSIM::OEN::FromWire (packet);
	      }
	    default:
	      NS_FATAL_ERROR ("Unsupported format");
	      return 0;
	  }

	  // exception will be thrown if packet is not recognized
      }
      catch (UnknownHeaderException)
      {
	  NS_FATAL_ERROR ("Unknown NNN header");
	  return 0;
      }
    }
  else
    {
      if (wireFormat == WIRE_FORMAT_NNNSIM)
	return wire::nnnSIM::OEN::FromWire (packet);
      else
	{
	  NS_FATAL_ERROR ("Unsupported format requested");
	  return 0;
	}
    }
}

///////////////////////////////////////////////////////////////////////////////
// Helper methods for Python
///////////////////////////////////////////////////////////////////////////////

std::string
Wire::FromNULLpStr (const NULLp* nullp_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  Packet* pkt = FromNULLp (nullp_p, wireFormat);
  std::string wire;
  wire.resize (pkt->GetSize ());
  pkt->CopyData (reinterpret_cast<uint8_t*> (&wire[0]), wire.size ());

  return wire;
}

NULLp*
Wire::ToNULLpStr (const std::string &wire, int8_t type/* = WIRE_FORMAT_AUTODETECT*/)
{
  Packet* pkt = new Packet (reinterpret_cast<const uint8_t*> (&wire[0]), wire.size ());
  return ToNULLp (pkt, type);
}

std::string
Wire::FromSOStr (const SO* so_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  Packet* pkt = FromSO (so_p, wireFormat);
  std::string wire;
  wire.resize (pkt->GetSize ());
  pkt->CopyData (reinterpret_cast<uint8_t*> (&wire[0]), wire.size ());

  return wire;
}

SO*
Wire::ToSOStr (const std::string &wire, int8_t type/* = WIRE_FORMAT_AUTODETECT*/)
{
  Packet* pkt = new Packet (reinterpret_cast<const uint8_t*> (&wire[0]), wire.size ());
  return ToSO (pkt, type);
}

std::string
Wire::FromDOStr (const DO* do_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  Packet* pkt = FromDO (do_p, wireFormat);
  std::string wire;
  wire.resize (pkt->GetSize ());
  pkt->CopyData (reinterpret_cast<uint8_t*> (&wire[0]), wire.size ());

  return wire;
}

DO*
Wire::ToDOStr (const std::string &wire, int8_t type/* = WIRE_FORMAT_AUTODETECT*/)
{
  Packet* pkt = new Packet (reinterpret_cast<const uint8_t*> (&wire[0]), wire.size ());
  return ToDO (pkt, type);
}

std::string
Wire::FromDUStr (const DU* du_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  Packet* pkt = FromDU (du_p, wireFormat);
  std::string wire;
  wire.resize (pkt->GetSize ());
  pkt->CopyData (reinterpret_cast<uint8_t*> (&wire[0]), wire.size ());

  return wire;
}

DU*
Wire::ToDUStr (const std::string &wire, int8_t type/* = WIRE_FORMAT_AUTODETECT*/)
{
  Packet* pkt = new Packet (reinterpret_cast<const uint8_t*> (&wire[0]), wire.size ());
  return ToDU (pkt, type);
}

std::string
Wire::FromENStr (const EN* en_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  Packet* pkt = FromEN (en_p, wireFormat);
  std::string wire;
  wire.resize (pkt->GetSize ());
  pkt->CopyData (reinterpret_cast<uint8_t*> (&wire[0]), wire.size ());

  return wire;
}

EN*
Wire::ToENStr (const std::string &wire, int8_t type/* = WIRE_FORMAT_AUTODETECT*/)
{
  Packet* pkt = new Packet (reinterpret_cast<const uint8_t*> (&wire[0]), wire.size ());
  return ToEN (pkt, type);
}

std::string
Wire::FromAENStr (const AEN* aen_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  Packet* pkt = FromAEN (aen_p, wireFormat);
  std::string wire;
  wire.resize (pkt->GetSize ());
  pkt->CopyData (reinterpret_cast<uint8_t*> (&wire[0]), wire.size ());

  return wire;
}

AEN*
Wire::ToAENStr (const std::string &wire, int8_t type/* = WIRE_FORMAT_AUTODETECT*/)
{
  Packet* pkt = new Packet (reinterpret_cast<const uint8_t*> (&wire[0]), wire.size ());
  return ToAEN (pkt, type);
}

std::string
Wire::FromRENStr (const REN* ren_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  Packet* pkt = FromREN (ren_p, wireFormat);
  std::string wire;
  wire.resize (pkt->GetSize ());
  pkt->CopyData (reinterpret_cast<uint8_t*> (&wire[0]), wire.size ());

  return wire;
}

REN*
Wire::ToRENStr (const std::string &wire, int8_t type/* = WIRE_FORMAT_AUTODETECT*/)
{
  Packet* pkt = new Packet (reinterpret_cast<const uint8_t*> (&wire[0]), wire.size ());
  return ToREN (pkt, type);
}

std::string
Wire::FromINFStr (const INF* inf_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  Packet* pkt = FromINF (inf_p, wireFormat);
  std::string wire;
  wire.resize (pkt->GetSize ());
  pkt->CopyData (reinterpret_cast<uint8_t*> (&wire[0]), wire.size ());

  return wire;
}

INF*
Wire::ToINFStr (const std::string &wire, int8_t type/* = WIRE_FORMAT_AUTODETECT*/)
{
  Packet* pkt = new Packet (reinterpret_cast<const uint8_t*> (&wire[0]), wire.size ());
  return ToINF (pkt, type);
}

std::string
Wire::FromOENStr (const OEN* oen_p, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  Packet* pkt = FromOEN (oen_p, wireFormat);
  std::string wire;
  wire.resize (pkt->GetSize ());
  pkt->CopyData (reinterpret_cast<uint8_t*> (&wire[0]), wire.size ());

  return wire;
}

OEN*
Wire::ToOENStr (const std::string &wire, int8_t type/* = WIRE_FORMAT_AUTODETECT*/)
{
  Packet* pkt = new Packet (reinterpret_cast<const uint8_t*> (&wire[0]), wire.size ());
  return ToOEN (pkt, type);
}

std::string
Wire::FromName (const NNNAddress* name, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{

  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    {
      Buffer buf;
      buf.AddAtStart (wire::NnnSim::SerializedSizeName (*name));
      Buffer::Iterator i = buf.Begin ();

      wire::NnnSim::SerializeName (i, *name);

      std::string wire;
      wire.resize (buf.GetSize ());
      buf.CopyData (reinterpret_cast<uint8_t *>(&wire[0]), wire.size ());

      return wire;
    }
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
      return "";
    }
}

NNNAddress*
Wire::ToName (const std::string &name, int8_t wireFormat/* = WIRE_FORMAT_DEFAULT*/)
{
  Buffer buf;
  buf.AddAtStart (name.size ());
  Buffer::Iterator i = buf.Begin ();
  i.Write (reinterpret_cast<const uint8_t *> (&name[0]), name.size ());

  i = buf.Begin ();

  if (wireFormat == WIRE_FORMAT_DEFAULT)
    wireFormat = GetWireFormat ();

  if (wireFormat == WIRE_FORMAT_NNNSIM)
    {
      return wire::NnnSim::DeserializeName (i);
    }
  else
    {
      NS_FATAL_ERROR ("Unsupported format requested");
    }
}

NNN_NAMESPACE_END

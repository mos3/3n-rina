#!/bin/bash


SPEEDS="1.40 2.80 4.20 5.60 7.00 8.40 9.80 11.20"
NSLOG=MobilityICC
NSscenario=nnn-icc-mobility
SCDE=01-001-004.txt

GRHDIR=./src/nnnsim/doc/graphing

CONSUMER=0
PRODUCER=7

T1="3n"
T2="smart"
STR1="NDN Smart Flooding"
STR2="3N Architecture"
DIR=results
OUTPUT=$DIR/graphs
LOSS=0.5
SEQ=20
EXTRA="--shape"

###############################################################################

speedstr=""
for i in $SPEEDS
do
    speed=${i//\./-}

    # Create comparison graphs
    # App Delay
    # Consumer Mobility
    echo "Creating Consumer mobility network delay $i"
    $GRHDIR/app-data-compare.R -r -d -f $DIR/$NSLOG-app-delays-$T2-con-$speed-$SCDE -c $DIR/$NSLOG-app-delays-$T1-con-$speed-$SCDE --str1="$STR1" --str2="$STR2" -t "Mobile Consumer" -s $SEQ --txt -o $OUTPUT

    # Producer Mobility
    echo "Creating Producer mobility network delay $i"
    $GRHDIR/app-data-compare.R -r -d -f $DIR/$NSLOG-app-delays-$T2-prod-$speed-$SCDE -c $DIR/$NSLOG-app-delays-$T1-prod-$speed-$SCDE --str1="$STR1" --str2="$STR2" -t "Mobile Producer" -s $SEQ --txt -o $OUTPUT

    # Aggregate packets
    # Consumer Mobility
    echo "Creating Consumer mobility Interest success rate $i"
    $GRHDIR/int-tr-compare.R -e $CONSUMER -f $DIR/$NSLOG-aggregate-trace-$T2-con-$speed-$SCDE -c $DIR/$NSLOG-aggregate-trace-$T1-con-$speed-$SCDE -t "Mobile consumer" --str1="$STR1" --str2="$STR2" -s $SEQ --txt -o $OUTPUT

    # Producer Mobility
    echo "Creating Producer mobility Interest success rate $i"
    $GRHDIR/int-tr-compare.R -e $PRODUCER -f $DIR/$NSLOG-aggregate-trace-$T2-prod-$speed-$SCDE -c $DIR/$NSLOG-aggregate-trace-$T1-prod-$speed-$SCDE -t "Consumer with Mobile Producer" -s $SEQ --str1="$STR1" --str2="$STR2" --txt -o $OUTPUT

    # Special Producer Mobility graph
    $GRHDIR/app-data.R -r -s $SEQ -d -f $DIR/$NSLOG-app-delays-$T1-prod-$speed-$SCDE -t "Application using 3N + Smart Flooding" --txt -o $OUTPUT

    # Data rate
    # Consumer Mobility
    echo "Mobile Consumer Data rate $i"
    $GRHDIR/rate-tr-icn-compare.R -e $CONSUMER -f $DIR/$NSLOG-rate-trace-$T2-con-$speed-$SCDE -c $DIR/$NSLOG-rate-trace-$T1-con-$speed-$SCDE -t "Mobile Consumer" -s $SEQ --str1="$STR1" --str2="$STR2" --txt -o $OUTPUT

    # Producer Mobility
    echo "Consumer with Mobile Producer Data rate $i"
    $GRHDIR/rate-tr-icn-compare.R -e $PRODUCER -f $DIR/$NSLOG-rate-trace-$T2-prod-$speed-$SCDE -c $DIR/$NSLOG-rate-trace-$T1-prod-$speed-$SCDE -t "Consumer with Mobile Producer" -s $SEQ --str1="$STR1" --str2="$STR2" --txt -o $OUTPUT

done

speedstr=${SPEEDS// /,}

echo "Data rate vs Speed graphs"
$GRHDIR/speed-avg-rate.R -s $NSLOG --str1="$STR1" --str2="$STR2" -m "$speedstr" --e1="$CONSUMER" --e2="$PRODUCER" -x $SCDE -d $DIR -p -c --txt -o $OUTPUT $EXTRA

echo "Max Seq, loss and Delay vs Speed graphs"
$GRHDIR/speed-avg-delay.R -s $NSLOG --str1="$STR1" --str2="$STR2" -m "$speedstr" -c -p --delay --loss="$LOSS" --maxSeq -x $SCDE -d $DIR --txt -o $OUTPUT $EXTRA


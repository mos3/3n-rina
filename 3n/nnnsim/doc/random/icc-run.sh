#!/bin/bash

SPEEDS="1.40 2.80 4.20 5.60 7.00 8.40 9.80 11.20"
NSscenario=nnn-icc-mobility

###############################################################################

speedstr=""
for i in $SPEEDS
do
    speed=${i//\./-}

    # Run 3N Consumer
    echo "Running 3N Mobile Consumer $i"
    ./waf --run "$NSscenario --mobile --speed=$i --trace"

    # Run 3N Producer
    echo "Running 3N Mobile Producer $i"
    ./waf --run "$NSscenario --mobile --speed=$i --producer --trace"

    # Run NDN Smart Flooding Consumer
    echo "Running NDN Mobile Consumer $i"
    ./waf --run "$NSscenario --speed=$i --trace"

    # Run NDN Smart Flooding Producer
    echo "Running NDN Mobile Producer $i"
    ./waf --run "$NSscenario --speed=$i --producer --trace"

done

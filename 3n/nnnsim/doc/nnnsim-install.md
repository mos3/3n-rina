# Install nnnsim with ns-3

The installation procedure seen below has been tested with Debian Jessie using ns-3 3.23.

1. Download the [ns-allinone-3.23.tar.bz2](https://www.nsnam.org/ns-3-23/)

2. Decompress the file to the desired directory

        $ tar xjvf ns-allinone-3.23.tar.bz2

3. Clone the nnnsim repository within the ns-allinone-3.23/ns-3.23/src directory

        ns-allinone-3.23/ns-3.23/src $ git clone https://github.com/nacarino/nnnsim.git

4. If you want to be able to use Point-To-Point NetDevices in your ns-3 scenarios, a patch is required.
    To apply the patch, you can use the following command line:

        ns-allinone-3.23/ns-3.23/src $ patch -p 0 < nnnsim/doc/patches/3n-icn-enabled-point-to-point.patch

5. Once patched, you can build ns-3 with nnnsim using the supplied build.py script in ns-allinone-3.23.
    nnnsim also includes examples which we highly recommend seeing before doing anything else.

    The following command line takes care of the configuration and compilation of ns-3

        ns-allinone-3.23 $ ./build.py --enable-examples

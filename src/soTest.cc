/*
 * soTest.cc
 *
 *  Created on: Nov 11, 2015
 *      Author: user1
 */

#include <iostream>
#include <string>

#include "ns3/nnn-icn-interest.h"
#include "ns3/nnn-so.h"
#include "ns3/ptr.h"

using namespace ns3;
using namespace std;

int main(int argc, char *argv[]){

	clog << "test" << endl;
	time_t now;
//	struct tm* time;
//	time->tm_sec = 2;

	now = time(NULL);

	Packet* payload = new Packet();
	nnn::Interest interest(payload);

	//	icn::Name* name = new icn::Name* (icn::Name(s));
	string name  = "interesrTest";
	interest.SetName(name);
	interest.SetInterestLifetime(5);
	interest.SetNack(2);
	interest.SetNonce(1);
	interest.SetScope(0);
	interest.Print(cout);

//	Packet interestPacket();
	clog << "\nSO test" << endl;
	string s = "/waseda/sato-lab/interest1";
	nnn::SO* so = new nnn::SO(&s, payload);
	so->SetLifetime(100);
	so->Print(cout);
//	so<<(&std::cout);

//	string address = "1.A.1";
//	Packet packet();
//	nnn::NNNAddress name = nnn::NNNAddress(&address);
//
//	Ptr<nnn::NNNAddress> leavingAddr = ::Create<NNNAddress> (den_p->GetNamePtr ()->getName ());
//
//	nnn::SO so = nnn::SO();
//	nnn::SO so2 = nnn::SO(name, &packet);

}
